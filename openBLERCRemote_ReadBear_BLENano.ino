#include <BLE_API.h>
#include <Servo.h>

#define TXRX_BUF_LEN 20
#define FORWARDPIN A3
#define REVERSEPIN A4
#define SERVOPIN D2

BLE ble;
Timeout timeout;
Servo servos[4];

uint8_t rx[2] = {0x00,0x00};
uint8_t prevRx[2] = {0x01,0x01};

static uint8_t rx_buf[TXRX_BUF_LEN];
static uint8_t rx_buf_num;
static uint8_t rx_state=0;

// The Nordic UART Service
static const uint8_t service1_uuid[]                = {0x71, 0x3D, 0, 0, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t service1_tx_uuid[]             = {0x71, 0x3D, 0, 3, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t service1_rx_uuid[]             = {0x71, 0x3D, 0, 2, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t uart_base_uuid_rev[]           = {0x1E, 0x94, 0x8D, 0xF1, 0x48, 0x31, 0x94, 0xBA, 0x75, 0x4C, 0x3E, 0x50, 0, 0, 0x3D, 0x71};

uint8_t tx_value[TXRX_BUF_LEN] = {0,};
uint8_t rx_value[TXRX_BUF_LEN] = {0,};

GattCharacteristic characteristic1(service1_tx_uuid, tx_value, 1, TXRX_BUF_LEN, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE );
GattCharacteristic characteristic2(service1_rx_uuid, rx_value, 1, TXRX_BUF_LEN, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY);
GattCharacteristic *uartChars[] = {&characteristic1, &characteristic2};
GattService uartService(service1_uuid, uartChars, sizeof(uartChars) / sizeof(GattCharacteristic *));


void disconnectionCallBack(Gap::Handle_t handle, Gap::DisconnectionReason_t reason)
{
  Serial.println("Disconnected!");
  Serial.println("Restarting the advertising process");
  ble.startAdvertising();
}

void writtenHandle(const GattWriteCallbackParams *Handler)
{
  uint8_t buf[TXRX_BUF_LEN];
  uint16_t bytesRead, index;

  if (Handler->handle == characteristic1.getValueAttribute().getHandle()) {

    ble.readCharacteristicValue(characteristic1.getValueAttribute().getHandle(), rx, &bytesRead);

    if (rx[0] != prevRx[0]) {
      //CH0 (direction) changée
      //~ Serial.print("Direction : ");

      if (((int) rx[0]) < 128) {
        //~ Serial.println(((int) rx[0])+90, DEC);
        servos[0].write(((int) rx[0])+90);
      } else {
        //~ Serial.println(((int) rx[0])-166, DEC);
        servos[0].write(((int) rx[0])-166);
      }
    }

    if (rx[1] != prevRx[1]) {
      //CH1 (accélération) changée
      if (((int8_t)rx[1]) == 0) {
        //~ Serial.println("Arret");
        analogWrite(REVERSEPIN, 0);
        analogWrite(FORWARDPIN, 0);
      } else if (((int8_t) rx[1]) > 0) {
        //~ Serial.print("Marche avant : ");
        //~ Serial.println(((int8_t) rx[1])*2, DEC);
        analogWrite(REVERSEPIN, 0);
        analogWrite(FORWARDPIN, ((int8_t) rx[1])*2);
      } else {
        //~ Serial.print("Marche arriere : ");
        //~ Serial.println(((int8_t) rx[1])*-2, DEC);
        analogWrite(FORWARDPIN, 0);
        analogWrite(REVERSEPIN, ((int8_t) rx[1])*-2);
      }
    }
    memcpy(prevRx, rx, 2);
  }
}


void setup() {

  pinMode(FORWARDPIN, OUTPUT);
  pinMode(REVERSEPIN, OUTPUT);
  digitalWrite(REVERSEPIN, LOW);
  digitalWrite(FORWARDPIN, LOW);

  servos[0].attach(SERVO0PIN);

  ble.init();
  ble.onDisconnection(disconnectionCallBack);
  ble.onDataWritten(writtenHandle);

  // setup adv_data and srp_data
  ble.accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED);
  ble.accumulateAdvertisingPayload(GapAdvertisingData::SHORTENED_LOCAL_NAME,
                                   (const uint8_t *)"OpenBLERC", sizeof("OpenBLERC") - 1);
  ble.accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_128BIT_SERVICE_IDS,
                                   (const uint8_t *)uart_base_uuid_rev, sizeof(uart_base_uuid_rev));

  ble.setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
  ble.addService(uartService);
  ble.setDeviceName((const uint8_t *)"OpenBLERC");
  // set tx power,valid values are -40, -20, -16, -12, -8, -4, 0, 4
  ble.setTxPower(4);
  // set adv_interval, 100ms in multiples of 0.625ms.
  ble.setAdvertisingInterval(160);
  ble.setAdvertisingTimeout(0);
  ble.startAdvertising();
}

void loop() {
  ble.waitForEvent();
}
