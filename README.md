# openBLERCremote - Redbear BLEnano receiver with brushed ESC

Bluetooth low energy receiver with integrated ESC for RC car on based on
an Redbear BLEnano.


## Hardware


### With lipo 1S (~ 3.7V)

Servo

* GND to lipo-
* V+ to lipo+
* Data to BLE Nano pin D2

ESC

![schema](mosfet_wired.png)


#### Example

* [OpenRC - 2WD micro crawler](http://www.thingiverse.com/thing:1706898)


## Transmitter

* [Android transmitter](https://gitlab.com/openBLERCremote/android_transmitter)
